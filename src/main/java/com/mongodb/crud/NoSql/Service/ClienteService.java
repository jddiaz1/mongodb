package com.mongodb.crud.NoSql.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.crud.NoSql.Entity.Cliente;
import com.mongodb.crud.NoSql.Repository.ClienteRepository;
import com.mongodb.crud.Validation.Entity.Error;
import com.mongodb.crud.Validation.Exception.NoFoundException;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente save(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    public String delete(String id) {
        clienteRepository.findById(id)
                .orElseThrow(() -> new NoFoundException("El id no existe",
                        new Error("Eliminar", "El id " + id + " no existe")));
        clienteRepository.deleteById(id);
        return "Registro eliminado";
    }

    public Cliente buscarNombre(String nombre) {
        return clienteRepository.buscarNombreCliente(nombre)
                .orElseThrow(() -> new NoFoundException("El id no existe",
                        new Error("Eliminar", "El nombre " + nombre + " no existe")));

    }

    public List<Cliente> buscarNombrePartial(String nombre) {
        return clienteRepository.buscarNombrePartial(nombre);

    }

    
}
