package com.mongodb.crud.NoSql.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.crud.NoSql.Entity.Cuenta;
import com.mongodb.crud.NoSql.Repository.CuentaRepository;
import com.mongodb.crud.Validation.Entity.Error;
import com.mongodb.crud.Validation.Exception.NoFoundException;

@Service
public class CuentaService {
    @Autowired
    CuentaRepository cuentaRepository;

    public Cuenta save(Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

    public List<Cuenta> findAll() {
        return cuentaRepository.findAll();
    }

    public String delete(String id) {
        cuentaRepository.findById(id)
                .orElseThrow(() -> new NoFoundException("Eliminar Registro",
                        new Error("Eliminar registro", "No existe regitro con id "+id)));
        cuentaRepository.deleteById(id);
        return "Registro eliminado";
    }

    public Cuenta update(Cuenta cuenta){

        return cuentaRepository.save(cuenta);
    }

}
