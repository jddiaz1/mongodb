package com.mongodb.crud.NoSql.Entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(value = "cuentas")
@Getter
@Setter
public class Cuenta {
    @Id
    private String idCuenta;
    private Date fechaCreacion;
    private double saldo;
    @DBRef
    @NotNull(message = "El valor es requerido")
    private Cliente cliente;
}
