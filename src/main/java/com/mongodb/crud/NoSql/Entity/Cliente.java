package com.mongodb.crud.NoSql.Entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(value = "clientes") 
@Setter
@Getter
public class Cliente implements Serializable {
    @Id
    @Size(min = 10,message = "El campo debe tener minimo 10 caracteres")
    private String idCliente;
    @NotBlank(message = "El dato no puede ser nulo")
    private String nombreCliente;
    private String correoCliente;
    @NotBlank(message = "El dato no puede ser nulo")
    private String claveCliente;
    
}
