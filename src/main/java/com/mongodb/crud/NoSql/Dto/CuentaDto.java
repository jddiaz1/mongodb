package com.mongodb.crud.NoSql.Dto;



import lombok.Data;

@Data
public class CuentaDto {
    private String id;
    private String nombre;
    private String correo;
    private String saldo;
    private String fecha;
}
