package com.mongodb.crud.NoSql.Dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonPropertyOrder({"correo","nombre","id"})
public class ClienteDto {
    private String id;
    private String nombre;
    private String correo;
}
