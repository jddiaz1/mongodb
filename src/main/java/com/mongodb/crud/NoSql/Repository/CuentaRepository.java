package com.mongodb.crud.NoSql.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.mongodb.crud.NoSql.Entity.Cuenta;

@Repository
public interface CuentaRepository extends MongoRepository<Cuenta,String> {
    
}
