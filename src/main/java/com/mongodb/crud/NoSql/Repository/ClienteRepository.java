package com.mongodb.crud.NoSql.Repository;



import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.crud.NoSql.Entity.Cliente;

@Repository
public interface ClienteRepository extends MongoRepository<Cliente, String> {
     @Query("{nombreCliente:?0}")
     public Optional<Cliente> buscarNombreCliente(String nombre);

     @Query("{nombreCliente:{ $regex : ?0 } }")
     public List<Cliente> buscarNombrePartial(String nombre);
}
