package com.mongodb.crud.NoSql.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.crud.NoSql.Dto.ClienteDto;
import com.mongodb.crud.NoSql.Entity.Cliente;
import com.mongodb.crud.NoSql.Service.ClienteService;
import com.mongodb.crud.Utility.ConvertEntity;
import com.mongodb.crud.Utility.Security.Hash;
import com.mongodb.crud.Validation.Exception.InvalidDataException;


@RestController
@RequestMapping("/api/v1/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    ClienteDto clienteDto = new ClienteDto();

    @Autowired
    ConvertEntity convertEntity;

    @PostMapping("/create")
    public Cliente save(@Valid @RequestBody Cliente cliente, BindingResult result) {
        if (result.hasErrors()) {
            throw new InvalidDataException("Datos errados", result);
        }
        cliente.setClaveCliente(Hash.sha1(cliente.getClaveCliente()));
        return clienteService.save(cliente);
    }

    @PutMapping("/update/{id}")
    public Cliente Update(@Valid @RequestBody Cliente cliente, BindingResult result,@PathVariable String id) {
        if (result.hasErrors()) {
            throw new InvalidDataException("Datos errados", result);
        }
        cliente.setIdCliente(id);
        cliente.setClaveCliente(Hash.sha1(cliente.getClaveCliente()));
        return clienteService.save(cliente);
    }

    @GetMapping("/list")
    public List<ClienteDto> findAll(){
        List<Cliente> cliente = clienteService.findAll();
        List<ClienteDto> lista = new ArrayList<>();
        for (Cliente cli : cliente) {
            lista.add((ClienteDto) convertEntity.convert(cli, clienteDto));
        }
        return lista;
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id){
        return clienteService.delete(id);
    }
   
    @GetMapping("/nombre/{nombre}")
    public ClienteDto findAll(@PathVariable String nombre){
       return (ClienteDto) convertEntity.convert(clienteService.buscarNombre(nombre), clienteDto);
    }

    @GetMapping("/nombreP/{nombre}")
    public List<ClienteDto> buscarNombre(@PathVariable String nombre){
        List<Cliente> cliente = clienteService.buscarNombrePartial(nombre);
        List<ClienteDto> lista = new ArrayList<>();
        for (Cliente cli : cliente) {
            lista.add((ClienteDto) convertEntity.convert(cli, clienteDto));
        }
        return lista;
    }

}
