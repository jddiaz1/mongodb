package com.mongodb.crud.NoSql.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.crud.NoSql.Dto.CuentaDto;
import com.mongodb.crud.NoSql.Entity.Cuenta;
import com.mongodb.crud.NoSql.Repository.ClienteRepository;
import com.mongodb.crud.NoSql.Service.CuentaService;
import com.mongodb.crud.Utility.ConvertEntity;
import com.mongodb.crud.Validation.Entity.Error;
import com.mongodb.crud.Validation.Exception.InvalidDataException;
import com.mongodb.crud.Validation.Exception.NoFoundException;

@RestController
@RequestMapping("/api/v1/cuenta")
public class CuentaController {

    @Autowired
    CuentaService cuentaService;

    CuentaDto cuentaDto = new CuentaDto();

    @Autowired
    ConvertEntity convertEntity;

    @Autowired
    ClienteRepository clienteRepository;
    @PostMapping("/create")
    public CuentaDto save(@Valid @RequestBody Cuenta cuenta,BindingResult result) {
        if (result.hasErrors()) {
            throw new InvalidDataException(result);
        } 
        clienteRepository.findById(cuenta.getCliente().getIdCliente())
        .orElseThrow(() -> new NoFoundException("El id no existe",
                new Error("Crear cuenta", "El id " + cuenta.getCliente().getIdCliente() + "del cliente no existe")));
        return (CuentaDto) convertEntity.convert(cuentaService.save(cuenta), cuentaDto);
    }

    @GetMapping("/list")
    public List<CuentaDto> findAll() {
        List<Cuenta> Cuenta = cuentaService.findAll();
        List<CuentaDto> lista = new ArrayList<>();
        for (Cuenta cli : Cuenta) {
            lista.add((CuentaDto) convertEntity.convert(cli, cuentaDto));
        }
        return lista;
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id){
        return cuentaService.delete(id);
    }

    @PostMapping("/update/{id}")
    public CuentaDto update(@Valid @RequestBody Cuenta cuenta,BindingResult result,@PathVariable String id) {
        
        if (result.hasErrors()) {
            throw new InvalidDataException(result);
        } 
        cuenta.setCliente(clienteRepository.findById(cuenta.getCliente().getIdCliente())
        .orElseThrow(() -> new NoFoundException("El id no existe",
                new Error("Crear cuenta", "El id " + cuenta.getCliente().getIdCliente() + "del cliente no existe"))));
        cuenta.setIdCuenta(id);
        return (CuentaDto) convertEntity.convert(cuentaService.update(cuenta), cuentaDto);
    }

}
