package com.mongodb.crud.MySql.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.crud.MySql.Entity.Cliente;
import com.mongodb.crud.MySql.Repository.ClienteRepositoryV2;

@Service
public class ClienteServiceV2 {
    @Autowired
    ClienteRepositoryV2 clienteRepository;

    public List<Cliente> findAll(){
        return (List<Cliente>) clienteRepository.findAll();
    }
}
