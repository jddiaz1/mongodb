package com.mongodb.crud.MySql.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.crud.MySql.Entity.Cliente;
import com.mongodb.crud.MySql.Service.ClienteServiceV2;

@RestController
@RequestMapping("/api/v2/cliente")
public class ClienteControllerV2 {
    
    @Autowired
    ClienteServiceV2 clienteService;

    @GetMapping("/listar")
    public List<Cliente> findAll(){
        return clienteService.findAll();
    }
}
