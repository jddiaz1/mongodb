package com.mongodb.crud.MySql.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mongodb.crud.MySql.Entity.Cliente;

@Repository
public interface ClienteRepositoryV2 extends CrudRepository<Cliente,String> {
    
}
